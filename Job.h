#ifndef JOB_H
#define JOB_H

#include <vector>

class Job {
public:
    explicit Job(std::vector<std::vector<int>> data, int s);
    Job();
    // 构造函数
    int makespan(std::vector<int> newRank); // // 加工总加工时长
    std::vector<int> getRank(); // 获取加工顺序
    void setRank(std::vector<int> newRank); // 重置加工顺序
    [[nodiscard]] int getSerial() const; // 获取序列号
    [[nodiscard]] size_t getN() const;  // 获取工件数量
    void printRank(); // 打印结果
private:
    int serial{}; // 序列号
    size_t n{};  // 工件数量
    size_t m{};  // 机器数量
    std::vector<std::vector<int>> costTime; // 工件在机器上加工所花费的时间
    std::vector<int> rank; // 加工顺序
};

#endif