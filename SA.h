#ifndef CODE_SA_H
#define CODE_SA_H

#include "Job.h"

class SA {
public:
    explicit SA(double startT, double endT, double alpha, int iter);  // 构造函数，设置初始值
    void run(Job& job);
private:
    double currentT; // 当前温度
    double endT; // 最终温度
    double alpha; // 降温率
    int iter; // 内循环迭代次数
    int bestValue; // 最优解的值
    std::vector<int> bestRank; // 最优解

    [[nodiscard]] bool accept(int oldTime, int newTime) const; // Metropolis准则
    static std::vector<int> generateNew(std::vector<int> oldRank); // 生成随机邻域解
};

#endif
