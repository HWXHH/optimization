#ifndef CODE_GA_H
#define CODE_GA_H

#include "Job.h"

class GA {
public:
    GA(size_t size, size_t n, double crossover, double mutation, size_t iter);
    void run(Job& job);
private:
    size_t size; // 种群规模
    std::vector<std::vector<int>> populations;   // 当前种群
    std::vector<std::vector<int>> parents;  // 父代种群
    std::vector<std::vector<int>> children; // 子代种群
    double POC; // 交叉概率
    double POM; // 突变概率
    std::vector<int> best;  // 最优解
    size_t iter{};    // 迭代次数

    void selectParent(Job& job);  // 选择父序列
    void crossover();   // 交叉
    void mutation();    // 突变
    void select(Job& job); // 筛选新种群
};

#endif
